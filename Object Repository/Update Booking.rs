<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Booking</name>
   <tag></tag>
   <elementGuidId>ee4aee4c-da07-4899-a7b7-f099d053fa7c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;firstname\&quot;: \&quot;Emma\&quot;,\n  \&quot;lastname\&quot;: \&quot;Watson\&quot;,\n  \&quot;totalprice\&quot;: 111,\n  \&quot;depositpaid\&quot;: true,\n  \&quot;bookingdates\&quot;: {\n    \&quot;checkin\&quot;: \&quot;2018-01-01\&quot;,\n    \&quot;checkout\&quot;: \&quot;2019-01-01\&quot;\n  },\n  \&quot;additionalneeds\&quot;: \&quot;Lunch\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>58575e10-add8-4a60-b348-31b2fc671927</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>b2b38079-102c-4a34-8633-3e11216a5561</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic YWRtaW46cGFzc3dvcmQxMjM=</value>
      <webElementGuid>f40f4e15-d830-42eb-b838-f38e213bb038</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.2</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://restful-booker.herokuapp.com/booking/23423</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'firstname', &quot;Emma&quot;)

WS.verifyElementPropertyValue(response, 'lastname', 'Watson')

WS.verifyElementPropertyValue(response, 'totalprice', 111)

WS.verifyElementPropertyValue(response, 'depositpaid', true)

WS.verifyElementPropertyValue(response, 'bookingdates.checkin', &quot;2018-01-01&quot;)

WS.verifyElementPropertyValue(response, 'bookingdates.checkout', &quot;2019-01-01&quot;)

WS.verifyElementPropertyValue(response, 'additionalneeds', 'Lunch')</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
