<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create Booking</name>
   <tag></tag>
   <elementGuidId>81dba8da-665d-4b8c-af63-0ba2709cf258</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;firstname\&quot;: \&quot;Kylie\&quot;,\n  \&quot;lastname\&quot;: \&quot;Ying\&quot;,\n  \&quot;totalprice\&quot;: 171,\n  \&quot;depositpaid\&quot;: false,\n  \&quot;bookingdates\&quot;: {\n    \&quot;checkin\&quot;: \&quot;2022-01-01\&quot;,\n    \&quot;checkout\&quot;: \&quot;2022-07-11\&quot;\n  },\n  \&quot;additionalneeds\&quot;: \&quot;Breakfast\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>4fce46de-e1cc-48ac-83a8-eaa7718c2da6</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>b53dae8f-a718-4f71-8b7f-699c9e4c5cab</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.2</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://restful-booker.herokuapp.com/booking</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import org.apache.logging.log4j.core.util.Assert

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable
import java.lang.Integer

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'booking.firstname', &quot;Kylie&quot;)

WS.verifyElementPropertyValue(response, 'booking.lastname', &quot;Ying&quot;)

WS.verifyElementPropertyValue(response, 'booking.totalprice', 171)

WS.verifyElementPropertyValue(response, 'booking.depositpaid', false)

WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkin', &quot;2022-01-01&quot;)

WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkout', &quot;2022-07-11&quot;)

WS.verifyElementPropertyValue(response, 'booking.additionalneeds', &quot;Breakfast&quot;)

def json = new JsonSlurper().parseText(response.getResponseText())

println(json.bookingid)

assertThat(json.bookingid instanceof Integer)
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
